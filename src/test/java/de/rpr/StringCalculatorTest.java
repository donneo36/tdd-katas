package de.rpr;

import org.junit.Test;

import java.security.SecureRandom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class StringCalculatorTest {

    @Test
    public void emptyStringIsZero() {
        assertEquals(0, new StringCalculator().add(""));
    }

    @Test
    public void oneNumberYieldsInput() {
        assertEquals(1, new StringCalculator().add("1"));
    }

    @Test
    public void twoNumbersYieldsSumOfInput() {
        assertEquals(3, new StringCalculator().add("1,2"));
    }

    @Test
    public void allowArbitraryNumberOfInput() {
        int amountInput = new SecureRandom().nextInt(10);
        StringBuilder builder = new StringBuilder();
        int expected = 0;
        for (int i = 0; i < amountInput + 1; i++) {
            builder.append(i).append(",");
            expected += i;
        }
        String input = builder.toString().substring(0, builder.length() - 1);
        assertEquals(expected, new StringCalculator().add(input));
    }

    @Test
    public void allowNewlineDelimiter() {
        assertEquals(6, new StringCalculator().add("1,2\n3"));
    }

    @Test
    public void useCustomDelimiters() {
        assertEquals(3, new StringCalculator().add("//;\n1;2"));
    }

    @Test
    public void negativeInputsYieldExceptionWithValueParameters() {
        try {
            new StringCalculator().add("1,2,-3,4,-5");
            fail("Should fail with exception!");
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("-3"));
            assertTrue(e.getMessage().contains("-5"));
        }
    }

    @Test
    public void ignoreNumbersBiggerThan1000() {
        assertEquals(3, new StringCalculator().add("1,2,1001"));
    }

}

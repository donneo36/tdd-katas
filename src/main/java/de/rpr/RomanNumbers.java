package de.rpr;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RomanNumbers {

    static List<Map.Entry<Integer, String>> romanSymbols = new ArrayList<>();

    static {
        romanSymbols.add(new AbstractMap.SimpleEntry<>(900, "CM"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(500, "D"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(400, "CD"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(100, "C"));

        romanSymbols.add(new AbstractMap.SimpleEntry<>(90, "XC"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(50, "L"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(40, "XL"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(10, "X"));

        romanSymbols.add(new AbstractMap.SimpleEntry<>(9, "IX"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(5, "V"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(4, "IV"));
        romanSymbols.add(new AbstractMap.SimpleEntry<>(1, "I"));
    }

    public static String convert(int arabicNumber) {
        for (Map.Entry<Integer, String> entry : romanSymbols) {
            if (arabicNumber >= entry.getKey()) {
                return entry.getValue() + convert(arabicNumber - entry.getKey());
            }
        }
        return "";
    }
}
